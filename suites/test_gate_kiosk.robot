*** Settings ***
Resource  keywords.robot

Test Setup  Open test browser
Test Teardown  Close test browser

*** Test Cases ***

Test Attendance Kiosk
    [Tags]  Login
    Login as Admin
    
    # Take a screen shot of the dashboard before doing the kiosk scan
    Scroll Element Into View    css=.small-box.student-present-panel
    Capture Page Screenshot
    
    Go to   ${SITE_URL}/check-in
    Wait Until Page Contains    Online Checkin
    
    # Get demo student card number
    ${student}  get random student card num
    
    # Perform a gate scan
    Input text      css=input[name=rfid]    100
    
    # Take a screen shot the scan
    Capture Page Screenshot
    
    # Perform the scan
    Submit Form     css=form
    Sleep   3s
    Run Keyword And Ignore Error    Textfield Value Should Be   css=input[name=rfid]    ${student}

    # Take a screen shot of the kiosk
    Capture Page Screenshot
    
    # Take a screen shot of the dashboard after the scan was done
    Go to   ${SITE_URL}/dashboard
    Scroll Element Into View    css=.small-box.student-present-panel
    Capture Page Screenshot