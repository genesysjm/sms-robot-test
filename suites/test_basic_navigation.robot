*** Settings ***
Resource  keywords.robot

Test Setup  Open test browser
Test Teardown  Close test browser

*** Keywords ***

Check Page Subtitle
    [Arguments]     ${subtitle}=List

    Element Should Contain      css=div.content-wrapper > section.content-header > h1 > small       ${subtitle}

Click Sidebar Menu By Index
    [Arguments]     ${index}    ${title}=Student

    Scroll Element Into View    css=.wrapper > aside.main-sidebar ul > li.treeview:nth-child(${index})
    Click Element   css=.wrapper > aside.main-sidebar ul > li.treeview:nth-child(${index})
    Wait Until Element Is Visible   css=aside.main-sidebar ul > li.treeview:nth-child(${index}).menu-open > ul > li:nth-child(1) > a   timeout=1.5 minutes
    Click Element   css=aside.main-sidebar ul > li.treeview:nth-child(${index}).menu-open > ul > li:nth-child(1) > a
    Wait Until Page Contains    ${title}

Click Student List Submenu
    [Arguments]     ${index}    ${title}=Student

    Wait Until Element Is Visible   css=li.treeview.student-tree.active > ul > li:nth-child(${index}) > a   timeout=1.5 minutes
    Click Element   css=li.treeview.student-tree.active > ul > li:nth-child(${index}) > a
    Wait Until Page Contains    ${title}

Click Submenu for Active Side Menu
    [Arguments]     ${index}    ${title}=Student List
    
    Wait Until Element Is Visible   css=li.active > ul > li:nth-child(${index}) > a   timeout=1.5 minutes
    Click Element   css=li.active > ul > li:nth-child(${index}) > a
    Wait Until Page Contains    ${title}

Click Submenu for Indexed Side Menu
    [Arguments]     ${parent_index}     ${index}    ${title}=Student List
    
    Wait Until Element Is Visible   css=li:nth-child(${parent_index}) > ul > li:nth-child(${index}) > a   timeout=1.5 minutes
    Click Element   css=li:nth-child(${parent_index}) > ul > li:nth-child(${index}) > a
    Wait Until Page Contains    ${title}


*** Test Cases ***

Test Student Side Submenu
    [Tags]  Login
    Login as Admin
    
    # Click on Student Side Menu
    Click Sidebar Menu By Index     3       Student
    Check Page Subtitle
    # Click Element   css=.wrapper > aside.main-sidebar ul > li.treeview.student-tree
    # Wait Until Page Contains Element    css=li.treeview.student-tree.menu-open > ul > li:nth-child(2) > a
    # Click Element   css=li.treeview.student-tree.menu-open > ul > li:nth-child(1) > a
    # Wait Until Page Contains    Student List
    
    # Click on PATH Student List Submenu
    # Wait Until Page Contains Element    css=li.treeview.student-tree.active > ul > li:nth-child(3) > a
    Click Student List Submenu      2
    
    # Click on Delinquents Submenu
    Click Student List Submenu      3
    
    # Click on Suspension List Submenu
    Click Student List Submenu      4
    
    # Click on Sanction List Submenu
    Click Student List Submenu      5
    
    # Click on the Student Offenses List Submenu
    Click Student List Submenu      6   Student
    
    # Click on the Student Watchlist Submenu
    Click Student List Submenu      7
    
    # Click on the Student Admission List Submenu
    Click Student List Submenu      8   Student Admission


Test Attendance Side Submenus
    [Tags]  Login
    Login as Admin
    
    # Click on Student Side Menu
    Click Sidebar Menu By Index     4        Student Attendance
    Check Page Subtitle
    
    # Click on the Student Attandance List Submenu
    Click Submenu for Active Side Menu      1   Student Attendance
    
    # Click on the PATH Student Attandance List Submenu
    Click Submenu for Active Side Menu      2   Student Attendance
    
    # Click on the Employee Attandance List Submenu
    Click Submenu for Active Side Menu      3   Student Attendance


Test Academic Side Submenus
    [Tags]  Login
    Login as Admin
    
    # Click on Academics Side Menu
    Click Sidebar Menu By Index     5        Grade
    Check Page Subtitle

    # Click on the Form Class Submenu
    Click Submenu for Active Side Menu      2   Form Class
    Check Page Subtitle
    
    # Click on the Subject List Submenu
    Click Submenu for Active Side Menu      3   Subject
    Check Page Subtitle


Test Exam Side Submenus
    [Tags]  Login
    Login as Admin
    
    # Click on Student Side Menu
    Click Sidebar Menu By Index     6   Exam
    Check Page Subtitle
    
    # Click on the Grade Submenu
    Click Submenu for Active Side Menu      2   Grade
    Check Page Subtitle
    
    # Click on the Exam Rules Submenu
    Click Submenu for Active Side Menu      3   Exam Rules
    Check Page Subtitle


Test Mark & Side Submenus
    [Tags]  Login
    Login as Admin
    
    # Click on Student Side Menu
    Click Sidebar Menu By Index     7   Marks
    Check Page Subtitle
    
    # Click on the Result Submenu
    Click Submenu for Active Side Menu      2   Result
    Check Page Subtitle


Test Message Centre Side Submenus
    [Tags]  Login
    Login as Admin

    # Click on Message Centre Side Menu
    Click Sidebar Menu By Index     8   SMS Messages


Test Lunch Submenus
    [Tags]  Login
    Login as Admin
    
    # Click on Lunch Side Menu
    Click Sidebar Menu By Index     9   Lunch Menu
    Check Page Subtitle


Test HRM Submenus
    [Tags]  Login
    Login as Admin
    
    # Click on HRM Menu
    Click Sidebar Menu By Index     10   Employee
    Check Page Subtitle
    
    # Click on the Leave Submenu
    Click Submenu for Active Side Menu      2   Leave
    Check Page Subtitle

    # Click on the Work Outside Submenu
    Click Submenu for Active Side Menu      3   Work Outside
    Check Page Subtitle

    # Click on the Policy Submenu
    Click Submenu for Active Side Menu      4   Policy Settings

Test Administrator Submenus
    [Tags]  Login
    Login as Admin
    
    # Click on Administrator Menu
    Click Sidebar Menu By Index     11   Academic Year
    Check Page Subtitle
    
    # Click on the Mail/SMS Template Submenu
    Click Submenu for Active Side Menu      2   Mail/SMS Template
    Check Page Subtitle
    
    # Click on the Idcard Template Submenu
    Click Submenu for Active Side Menu      3   Idcard Template
    Check Page Subtitle

    # Click on the System Admin Submenu
    Click Submenu for Active Side Menu      4   System Admin

    # Click on the Reset User Password Submenu
    Click Submenu for Active Side Menu      5   System Admin

    # Click on the Role Submenu
    Click Submenu for Active Side Menu      4   Role


Test Users Menu
    [Tags]  Login
    Login as Admin
    
    # Click on Users Menu
    Click Element   css=.wrapper > aside.main-sidebar ul > li:nth-child(12)
    Wait Until Page Contains    User
    Check Page Subtitle


Test Settings Menu
    [Tags]  Login
    Login as Admin
    
    # Click on Users Menu
    Click Sidebar Menu By Index     14   Institute Settings
    
    # # Click on the Academic Calendar Submenu
    # Click Submenu for Active Side Menu      2   Academic Calendar
    # Check Page Subtitle
    
    # # Click on the SMS Gateway Submenu
    # Click Submenu for Active Side Menu      3   SMS Gateway
    # Check Page Subtitle

    # # Click on the Report Settings Submenu
    # Click Submenu for Active Side Menu      4   Report Settings