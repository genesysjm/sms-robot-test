*** Settings ***
Library     Selenium2Library
Library     XvfbRobot
Library     DynamicTestCases.py
Library     fakeData.py


*** Variables ***
${ROBOT_USER_ADMIN}     robottestadmin
${ROBOT_USER_DEAN}     robottestdean
${ROBOT_USER_MANAGER}     robottestmanager
${ROBOT_USER_PRINCIPAL}     robottestprincipal
${ROBOT_USER_SECURITY}     robottestsecurity
${ROBOT_USER_TEACHER}     robottestteacher
${ROBOT_USERNAME}  ${ROBOT_USER_ADMIN}
${ROBOT_PASSWORD}   testuser123
@{ROBOT_USERS}      ${ROBOT_USER_ADMIN} ${ROBOT_USER_DEAN}  ${ROBOT_USER_MANAGER}   ${ROBOT_USER_PRINCIPAL} ${ROBOT_USER_SECURITY}  ${ROBOT_USER_TEACHER}
${LOGIN_USER}   ${ROBOT_USERNAME}
${LOGIN_ADMIN_SUCCESS_MSG}  Welcome
${SITE_URL}  http://localhost:8080
${ROBOT_PASSWORD}  ${ROBOT_PASSWORD}
${LOGIN_URL}    ${SITE_URL}/@@login
${TMP_PATH}                 /tmp

*** Keywords ***
Pause
    [Documentation]  Visually pause test execution with interactive dialog by
    ...              importing **Dialogs**-library and calling its
    ...              **Pause Execution**-keyword.
    Import library  Dialogs
    Pause execution

# ----------------------------------------------------------------------------
# Access Resources
# ----------------------------------------------------------------------------

Go to homepage
    Go to   ${SITE_URL}
    Wait until location is  ${SITE_URL}

# ----------------------------------------------------------------------------
# Login/Logout
# ----------------------------------------------------------------------------

Open Chrome
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size  1400  768

Open test browser
    Open Chrome
    Go To    ${SITE_URL}

Close test browser
    Run Keyword And Ignore Error  Close browser

Login
    [Documentation]  Log in to the site as ${username} using ${password}. There
    ...              is no guarantee of where in the site you are once this is
    ...              done. (You are responsible for knowing where you are and
    ...              where you want to be)
    [Arguments]     ${username}     ${password}

    Go to  ${SITE_URL}
    Set Global Variable     ${LOGIN_USER}       ${username}

    Wait Until Page Contains Element  css=#loginForm  timeout=0.5 minutes
    Page should contain element  css=input[name='username']
    Page should contain element  css=input[name='password']

    Input text  css=input[name='username']  ${username}
    Input text  css=input[name='password']  ${password}
    Click button  css=#loginForm button[type="submit"]
    Wait Until Page Contains Element  css=aside.main-sidebar > div > section > ul > li.sidebar-dashboard.active  timeout=0.5 minutes
    Page should contain  Welcome to

Login as Admin
    Login   ${ROBOT_USER_ADMIN}     ${ROBOT_PASSWORD}

Login as Dean
    Login   ${ROBOT_USER_DEAN}     ${ROBOT_PASSWORD}

Login as Manager
    Login   ${ROBOT_USER_MANAGER}     ${ROBOT_PASSWORD}

Login as Security
    Login   ${ROBOT_USER_SECURITY}     ${ROBOT_PASSWORD}

Login as Teacher
    Login   ${ROBOT_USER_TEACHER}     ${ROBOT_PASSWORD}

Login as Principal
    Login   ${ROBOT_USER_PRINCIPAL}     ${ROBOT_PASSWORD}


Go to Dashboard
    Go to   ${SITE_URL}/dashboard
    Wait Until Page Contains Element  css=aside.main-sidebar > div > section > ul > li.sidebar-dashboard.active  timeout=0.5 minutes
    Element Should Contain      css=.students-panel     Students

    Element Should Contain      css=.student-after-hours-panel     Evening Classes
    Element Should Contain      css=.student-after-hours-panel   Today

Log out
    Go to  ${SITE_URL}/logout
    Page Should Contain     Your are now logged out!
    Page Should Contain Element  css=#loginForm


Select Custom Checkbox
    [Arguments]     ${locator}
    Execute JavaScript    $("${locator}").attr("checked", "checked").parent().addClass('checked');


Unselect Custom Checkbox
    [Arguments]     ${locator}
    Execute JavaScript    $("${locator}").removeAttr("checked", "false").parent().removeClass('checked');


Date Input
    [Arguments]     ${css}     ${text}
    Execute JavaScript  $("${css}").val("${text}")

Select2 Input
    [Arguments]     ${css}     ${text}
    Execute Javascript   $("${css}").val("${text}"); $("${css}").select2().trigger('change');
    
    
Select2 Form Class
    [Arguments]     ${locator}     ${position}
    
    Run Keyword And Ignore Error         Wait Until Page Contains Element    css=${locator} option:nth-child(${position}
    Execute Javascript   var option = $("${locator} option:nth-child(${position}").val(); $("${locator}").val(option); $("${locator}").select2().trigger('change');
