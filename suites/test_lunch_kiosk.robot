*** Settings ***
Resource  keywords.robot

Test Setup  Open test browser
Test Teardown  Close test browser

*** Test Cases ***

Test Lunch Kiosk
    [Tags]  Login
    Login as Admin
    
    # Take a screen shot of the lunch listing before the scan
    Go to   ${SITE_URL}/lunch/menu
    Wait Until Page Contains Element    css=section.content-header > ol > li
    Wait Until Element Contains     css=section.content-header > ol > li.active  Lunch Menu
    Capture Page Screenshot
    
    Go to   ${SITE_URL}/lunch/ticket/scan
    Wait Until Page Contains    Online Lunch Collection
    
    # Get demo student card number
    ${student}  get random student card num
    
    # Perform a gate scan
    Input text      css=input[name=rfid]    ${student}
    
    # Take a screen shot the scan
    Capture Page Screenshot
    
    # Perform the scan
    Submit Form     css=form
    Sleep   3s
    Run Keyword And Ignore Error    Textfield Value Should Be   css=input[name=rfid]    ${student}
    
    # Take a screen shot of the kiosk
    Capture Page Screenshot
    
    # Take a screen shot of the lunch listing after the scan was done
    Go to   ${SITE_URL}/lunch/menu
    Wait Until Page Contains Element    css=section.content-header > ol > li
    Wait Until Element Contains     css=section.content-header > ol > li.active  Lunch Menu
    Capture Page Screenshot