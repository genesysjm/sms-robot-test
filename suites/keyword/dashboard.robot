*** Keywords ***
Page Should Contain School Profile Panels
    # General Panels
    Page Should Contain Element   css=section.content .students-panel a
    Page Should Contain Element   css=section.content .teachers-panel a
    Page Should Contain Element   css=section.content .employees-panel a
    Page Should Contain Element   css=section.content .parents-panel a
    Page Should Contain Element   css=section.content .path-students-panel a
    Page Should Contain Element   css=section.content .subjects-panel a
    Page Should Contain Element   css=section.content .box.grade-wise-student-panel
    Page Should Contain Element   css=section.content .box.class-wise-student-panel

Page Should Not Contain School Profile Panels
    # General Panels
    Page Should Not Contain Element   css=section.content .students-panel a
    Page Should Not Contain Element   css=section.content .teachers-panel a
    Page Should Not Contain Element   css=section.content .employees-panel a
    Page Should Not Contain Element   css=section.content .parents-panel a
    Page Should Not Contain Element   css=section.content .path-students-panel a
    Page Should Not Contain Element   css=section.content .subjects-panel a
    Page Should Not Contain Element   css=section.content .box.grade-wise-student-panel
    Page Should Not Contain Element   css=section.content .box.class-wise-student-panel

Page Should Contain Sanction Panels
    Page Should Contain Element   css=section.content .suspensions-panel a
    Page Should Contain Element   css=section.content .deliquents-panel a
    Page Should Contain Element   css=section.content .warnings-panel a
    Page Should Contain Element   css=section.content .watchlist-panel a

Page Should Not Contain Sanction Panels
    Page Should Not Contain Element   css=section.content .suspensions-panel a
    Page Should Not Contain Element   css=section.content .deliquents-panel a
    Page Should Not Contain Element   css=section.content .warnings-panel a
    Page Should Not Contain Element   css=section.content .watchlist-panel a

Page Should Contain Attendance Statistics Panels
    # Today's Statistics Panels
    Page Should Contain Element   css=section.content .student-present-panel a
    Page Should Contain Element   css=section.content .student-on-time-panel a
    Page Should Contain Element   css=section.content .student-early-panel a
    Page Should Contain Element   css=section.content .student-late-panel a
    Page Should Contain Element   css=section.content .student-absent-panel a
    Page Should Contain Element   css=section.content .student-leaving-panel a
    Page Should Contain Element   css=section.content .student-after-hours-panel a

Page Should Not Contain Attendance Statistics Panels
    # Today's Statistics Panels
    Page Should Not Contain Element   css=section.content .student-present-panel a
    Page Should Not Contain Element   css=section.content .student-on-time-panel a
    Page Should Not Contain Element   css=section.content .student-early-panel a
    Page Should Not Contain Element   css=section.content .student-late-panel a
    Page Should Not Contain Element   css=section.content .student-absent-panel a
    Page Should Not Contain Element   css=section.content .student-leaving-panel a
    Page Should Not Contain Element   css=section.content .student-after-hours-panel a

Page Should Contain SMS Chart Panel
    # SMS Graph
    Page Should Contain Element   css=section.content .box.sms-graph-panel
    Page Should Contain Element   css=#smsChart

Page Should Not Contain SMS Chart Panel
    # SMS Graph
    Page Should Not Contain Element   css=section.content .box.sms-graph-panel
    Page Should Not Contain Element   css=#smsChart