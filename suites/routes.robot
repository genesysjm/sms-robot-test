*** Settings ***
Resource  keywords.robot

*** Keywords ***


Navigation to Grade 7 Students Page
    Go to dashboard
    Click Element   css=section.content .students-panel a
    Wait Until Page Contains     Grade Seven
    Page Should Contain Element   css=div.small-box a.grade-Seven
    Page Should Contain Element   css=div.small-box a.grade-Eight
    Page Should Contain Element   css=div.small-box a.grade-Nine
    Page Should Contain Element   css=div.small-box a.grade-Ten
    Page Should Contain Element   css=div.small-box a.grade-Eleven
    Click Element   css=div.small-box a.grade-Seven
    Wait Until Page Contains Element    css=#listDataTableWithSearch_wrapper

Navigation to Teachers Page
    Go to dashboard
    Click Element   css=section.content .teachers-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Teacher

Navigation to Employees Page
    Go to dashboard
    Click Element   css=section.content .employees-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Employee

Navigation to Parents Page
    Go to dashboard
    Click Element   css=section.content .parents-panel a
    Wait Until Page Contains Element    css=#listDataTable
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   User
    Element Should Contain     css=#listDataTable > tbody > tr.odd > td:nth-child(6)    Parent

Navigation to Subjects Page
    Go to dashboard
    Click Element   css=section.content .subjects-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Subject

Navigation to Path Students Page
    Go to dashboard
    Click Element   css=section.content .path-students-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student List

Navigation to Suspended Students Page
    Go to dashboard
    Click Element   css=section.content .suspensions-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student List

Navigation to Delinquent Students Page
    Go to dashboard
    Click Element   css=section.content .delinquents-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student List

Navigation to Warnings Students Page
    Go to dashboard
    Click Element   css=section.content .warnings-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student List

Navigation to Watchlist Students Page
    Go to dashboard  
    Click Element   css=section.content .watchlist-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student List

Navigate to Grade Wise Student Pages
    Go to dashboard
    Scroll Element Into View    css=.grade-wise-student-panel ul > li > a
    ${num_grades}   Get Element Count   css=.grade-wise-student-panel ul > li > a
    ${random_num}   Evaluate    random.randint(0, ${num_grades})    modules=random
    Log     ${random_num}
    Execute JavaScript  document.querySelector('.grade-wise-student-panel ul > li:nth-child(${random_num}) > a').scrollIntoView(false); window.scrollBy(0,150);
    Click Element   css=.grade-wise-student-panel ul > li:nth-child(${random_num}) > a
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student List

Navigate to Class Wise Student Pages
    Go to dashboard
    Scroll Element Into View    css=.class-wise-student-panel ul > li > a
    ${num_grades}   Get Element Count   css=.class-wise-student-panel ul > li > a
    ${random_num}   Evaluate    random.randint(0, ${num_grades})    modules=random
    Log     ${random_num}
    Execute JavaScript  document.querySelector('.class-wise-student-panel ul > li:nth-child(${random_num}) > a').scrollIntoView(false); window.scrollBy(0,200);
    Click Element   css=.class-wise-student-panel ul > li:nth-child(${random_num}) > a
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student List
    
Navigation to Present Students Page
    Go to dashboard
    Scroll Element Into View    css=section.content .student-present-panel a
    Click Element   css=section.content .student-present-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student Attendance

Navigation to On-Time Students Page
    Go to dashboard
    Scroll Element Into View    css=section.content .student-on-time-panel a
    Click Element   css=section.content .student-on-time-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student Attendance

Navigation to Early Students Page   
    Go to dashboard
    Scroll Element Into View    css=section.content .student-early-panel a
    Click Element   css=section.content .student-early-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student Attendance

Navigation to Late Students Page
    Go to dashboard
    Scroll Element Into View    css=section.content .student-late-panel a
    Click Element   css=section.content .student-late-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student Attendance

Navigation to Absent Students Page
    Go to dashboard
    Scroll Element Into View    css=section.content .student-absent-panel a
    Click Element   css=section.content .student-absent-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student Attendance

Navigation to Leaving Students Page
    Go to dashboard
    Scroll Element Into View    css=section.content .student-leaving-panel a
    Click Element   css=section.content .student-leaving-panel a
    Sleep   3s
    Page Should Contain Element     css=section.content .student-leaving-panel a
    
Navigation to Evening Classes Students Page
    Go to dashboard
    Scroll Element Into View    css=section.content .student-after-hours-panel a
    Click Element   css=section.content .student-after-hours-panel a
    Sleep   3s
    Page Should Contain Element     css=section.content .student-after-hours-panel a

Navigation to Path Students Attendance Page
    Go to dashboard
    Scroll Element Into View    css=section.content .path-attendance-students-panel a
    Click Element   css=section.content .path-attendance-students-panel a
    Wait Until Page Contains Element    css=#listDataTableWithSearch
    Element Should Contain     css=div.content-wrapper > section.content-header > h1   Student Attendance
