import random
from faker import Faker


class fakeData(object):
    
    def __init__(self):
        self.demo_student_card_nums = ['DEMO{}'.format(n) for n in range(1,41)]
    
    def get_demo_student_card_nums(self):
        return self.demo_student_card_nums

    def get_random_student_card_num(self):
        return self.demo_student_card_nums[random.randint(0,39)]
    
    def get_fake_admission(self):
        fake = Faker()
        path_option = random.randint(0,1)
        return {
            'name': fake.name(),
            'address': fake.address(),
            'card_no': fake.ean8(),
            'dob': fake.date_between(start_date='-18y', end_date='-10y').strftime('%d/%m/%Y'),
            'gender_option': random.randint(1,3),
            'phone_no': '876{}'.format(random.randint(2000000, 9900000)),
            'father_name': fake.name(),
            'father_phone_no': '876{}'.format(random.randint(2000000, 9900000)),
            'mother_name': fake.name(),
            'mother_phone_no': '876{}'.format(random.randint(2000000, 9900000)),
            'grade_option': random.randint(2,6),
            'path_option': path_option,
            'form_class_option': random.randint(0,1),
            'path_code': '' if not path_option else fake.ean8() if random.randint(0,100)%2 else ''
        }