*** Settings ***

Resource  routes.robot

Test Setup  Open test browser
Test Teardown  Close test browser

*** Test Cases ***
Test Admin Dashboard Navigations
    Login as Admin
    Navigation to Grade 7 Students Page
    Navigation to Teachers Page
    Navigation to Employees Page
    Navigation to Parents Page
    Navigation to Subjects Page
    Navigation to Path Students Page
    Navigation to Suspended Students Page
    Navigation to Delinquent Students Page
    Navigation to Warnings Students Page
    Navigation to Watchlist Students Page
    Navigation to Present Students Page
    Navigation to On-Time Students Page
    Navigation to Early Students Page
    Navigation to Late Students Page
    Navigation to Absent Students Page
    Navigation to Leaving Students Page
    Navigation to Evening Classes Students Page
    Navigation to Path Students Attendance Page
    Navigate to Grade Wise Student Pages
    Navigate to Class Wise Student Pages

Test Dean Dashboard Navigations
    Login as Dean
    Navigation to Grade 7 Students Page
    Navigation to Teachers Page
    Navigation to Employees Page
    Navigation to Parents Page
    Navigation to Subjects Page
    Navigation to Path Students Page
    Navigation to Suspended Students Page
    Navigation to Delinquent Students Page
    Navigation to Warnings Students Page
    Navigation to Watchlist Students Page
    Navigation to Present Students Page
    Navigation to On-Time Students Page
    Navigation to Early Students Page
    Navigation to Late Students Page
    Navigation to Absent Students Page
    Navigation to Leaving Students Page
    Navigation to Evening Classes Students Page
    Navigation to Path Students Attendance Page

Test Manager Dashboard Navigations
    Login as Manager
    Navigation to Grade 7 Students Page
    Navigation to Teachers Page
    Navigation to Employees Page
    Navigation to Parents Page
    Navigation to Subjects Page
    Navigation to Path Students Page
    Navigation to Suspended Students Page
    Navigation to Delinquent Students Page
    Navigation to Warnings Students Page
    Navigation to Watchlist Students Page
    Navigation to Present Students Page
    Navigation to On-Time Students Page
    Navigation to Early Students Page
    Navigation to Late Students Page
    Navigation to Absent Students Page
    Navigation to Leaving Students Page
    Navigation to Evening Classes Students Page
    Navigation to Path Students Attendance Page

Test Principal Dashboard Navigations
    Login as Principal
    Navigation to Grade 7 Students Page
    Navigation to Teachers Page
    Navigation to Employees Page
    Navigation to Parents Page
    Navigation to Subjects Page
    Navigation to Path Students Page
    Navigation to Suspended Students Page
    Navigation to Delinquent Students Page
    Navigation to Warnings Students Page
    Navigation to Watchlist Students Page
    Navigation to Present Students Page
    Navigation to On-Time Students Page
    Navigation to Early Students Page
    Navigation to Late Students Page
    Navigation to Absent Students Page
    Navigation to Leaving Students Page
    Navigation to Evening Classes Students Page
    Navigation to Path Students Attendance Page

Test Security Dashboard Navigations
    Login as Security
    ${status} =     Run Keyword And Return Status   Navigation to Grade 7 Students Page
    Run Keyword If	'${status}' == 'PASS'   Log     Passed
    Navigation to Teachers Page
    Navigation to Employees Page
    Navigation to Parents Page
    Navigation to Subjects Page
    Navigation to Path Students Page
    Navigation to Suspended Students Page
    Navigation to Delinquent Students Page
    Navigation to Warnings Students Page
    Navigation to Watchlist Students Page
    Navigation to Present Students Page
    Navigation to On-Time Students Page
    Navigation to Early Students Page
    Navigation to Late Students Page
    Navigation to Absent Students Page
    Navigation to Leaving Students Page
    Navigation to Evening Classes Students Page
    Navigation to Path Students Attendance Page

Test Teacher Dashboard Navigations
    Login as Teacher
    Navigation to Grade 7 Students Page
    Navigation to Teachers Page
    Navigation to Employees Page
    Navigation to Parents Page
    Navigation to Subjects Page
    Navigation to Path Students Page
    Navigation to Suspended Students Page
    Navigation to Delinquent Students Page
    Navigation to Warnings Students Page
    Navigation to Watchlist Students Page
    Navigation to Present Students Page
    Navigation to On-Time Students Page
    Navigation to Early Students Page
    Navigation to Late Students Page
    Navigation to Absent Students Page
    Navigation to Leaving Students Page
    Navigation to Evening Classes Students Page
    Navigation to Path Students Attendance Page