*** Settings ***
Resource  keywords.robot

Test Setup  Open test browser
Test Teardown  Teardown Test Browser


*** Keywords ***

Teardown Test Browser
    Close test browser

Go the Student Admssion Page and Add an admission
    [Arguments]     ${admission}    ${form_class_option}

    Go to   ${SITE_URL}/student/admission
    
    Wait Until Page Contains Element    css=button.statusChangeHolder[name="bulk_admit"]
    Capture Page Screenshot
    
    # Test the Add New Button
    Click Element   css=div.box-header > div.box-tools.pull-right > a.add-new
    Wait Until Page Contains Element    css=#entryForm
    Capture Page Screenshot
    
    # Input the fake data
    Date Input  input[name='dob']   ${admission}[dob]
    Input Text  css=input[name='name']  ${admission}[name]
    Select2 Input   select[name='gender']   ${admission}[gender_option]
    Input Text  css=textarea[name="present_address"]    ${admission}[address]
    Input Text  css=input[name='card_no']   ${admission}[card_no]
    Select2 Input   select[name='class_id']    ${admission}[grade_option]
    Select2 Form Class      select[name='section_id']   ${form_class_option}
    Capture Page Screenshot

    Select2 Input   select[name='is_path']    ${admission}[path_option]
    Input Text  css=input[name='pathcode']   ${admission}[path_code]
    Select Custom Checkbox     input[name=verified]

    # Submit the Form
    Submit Form     css=#entryForm
    Wait Until Page Contains     Student admission number is
    Capture Page Screenshot

*** Test Cases ***

Test Student Admssion from the Admssion Listing Page
    [Tags]  Login
    Login as Admin
    ${admission}    get fake admission
    Log Variables
    Go the Student Admssion Page and Add an admission   ${admission}    2
    
    # Go to the admission listing page
    Click Element   css=section.content-header > ol > li:nth-child(3) > a
    Wait Until Page Does Not Contain Element    css=#entryForm
    Wait Until Page Contains Element    css=button.statusChangeHolder[name="bulk_admit"]
    Page Should Contain Element     css=button[name="show_admitted"]
    Capture Page Screenshot
    
    # Admit student
    # Show other fields when in table mode
    Scroll Element Into View    css=tr[class*='${admission}[card_no]']
    Click Element   css=tr[class*='${admission}[card_no]'] td:first-child
    Click Element   css=tr[class*='${admission}[card_no]'] .admit .toggle
    Wait Until Page Contains     Student added!
    Capture Page Screenshot
    
    # Ensure that students can't be readmitted
    Click Element   css=tr[class*='${admission}[card_no]'] .admit .toggle
    Wait Until Page Contains     Student already admitted!
    Capture Page Screenshot
    
    # Test the Show / Hide Admitted Students Button by hiding all admitted students
    Scroll Element Into View    css=.breadcrumb    
    Element Should Contain  css=button[name="show_admitted"]    Show Admitted Students
    Click Element   css=button[name="show_admitted"][value="1"]
    Wait Until Page Contains Element    css=button[name="show_admitted"][value="0"]
    Element Should Contain  css=button[name="show_admitted"]    Hide Admitted Students
    Scroll Element Into View    css=tr[class*='${admission}[card_no]']
    Page Should Contain Element     css=tr[class*='${admission}[card_no]']

    # Test the Show / Hide Admitted Students Button by showing all admitted students
    Scroll Element Into View    css=.breadcrumb   
    Click Element   css=button[name="show_admitted"][value="0"]
    Wait Until Page Contains Element    css=button[name="show_admitted"][value="1"]
    Element Should Contain  css=button[name="show_admitted"]    Show Admitted Students
    Page Should Not Contain Element     css=tr[class*='${admission}[card_no]']
    Capture Page Screenshot


Test Student Admssion from the Individual Student Admssion Page and Delete Admssion Afterwards
    [Tags]  Login
    Login as Admin
    ${admission}    get fake admission
    Log Variables
    Go the Student Admssion Page and Add an admission   ${admission}    0
    
    # Go to the admission listing page
    Click Element   css=section.content-header > ol > li:nth-child(3) > a
    Wait Until Page Does Not Contain Element    css=#entryForm
    Wait Until Page Contains Element    css=button.statusChangeHolder[name="bulk_admit"]
    Page Should Contain Element     css=button[name="show_admitted"]
    
    # Show other fields when in table mode
    Scroll Element Into View    css=tr[class*='${admission}[card_no]']
    Click Element   css=tr[class*='${admission}[card_no]'] td:first-child
    # Try to admit the student without placing the student in a form class.
    # This should fail and give a different already that Student added
    Click Element   css=tr[class*='${admission}[card_no]'] .admit .toggle
    Wait Until Page Contains     Please place the student in a Form Class before continuing!
    Capture Page Screenshot
    
    # Navigate to the Student edit page
    Click Element   css=tr[class*='${admission}[card_no]'] a[title="Edit"]
    Wait Until Page Contains Element    css=#entryForm
    Scroll Element Into View    css=select[name='section_id']
    
    # Set the form class to option two in the form class selection field
    Select2 Form Class      select[name='section_id']   2
    # Submit the Form
    Submit Form     css=#entryForm
    Wait Until Page Does Not Contain Element    css=#entryForm
    Wait Until Page Contains    Admission updated!
    Wait Until Page Contains Element    css=section.content-header .admission-card
    Page Should Not Contain     Registration No
    Capture Page Screenshot
    
    # Admit the Student
    Click Element   css=ul.list-group-unbordered .admit .toggle
    Wait Until Page Contains     Student added!
    # Refresh Page
    Reload Page
    Wait Until Page Contains Element    css=section.content-header .admission-card
    Wait Until Page Contains     Registration No
    Capture Page Screenshot
    
    # Go to the admission listing page
    Click Element   css=section.content-header > ol > li:nth-child(3) > a
    Wait Until Page Does Not Contain Element    css=section.content-header .admission-card
    Wait Until Page Contains Element    css=button.statusChangeHolder[name="bulk_admit"]
    Element Should Contain  css=button[name="show_admitted"]    Show Admitted Students
    Page Should Not Contain Element     css=tr[class*='${admission}[card_no]']

    # Test the Show / Hide Admitted Students Button by showing all admitted students
    Scroll Element Into View    css=.breadcrumb   
    Click Element   css=button[name="show_admitted"][value="1"]
    Wait Until Page Contains Element    css=button[name="show_admitted"][value="0"]
    Element Should Contain  css=button[name="show_admitted"]    Hide Admitted Students
    Scroll Element Into View    css=tr[class*='${admission}[card_no]']
    Page Should Contain Element     css=tr[class*='${admission}[card_no]']
    Capture Page Screenshot
    
    ${acode}    Get Text    css=tr[class*='${admission}[card_no]'] td.admission-code
    Page Should Contain     ${acode}
    
    # Test the cancelation of admission of the admitted.
    Click Element   css=button[type="submit"][title="Delete"]
    Wait Until Page Contains    Are you sure?
    Capture Page Screenshot
    Click Element   css=.swal2-cancel.swal2-styled
    Wait Until Page Does Not Contain    Are you sure?
    Page Should Contain Element     css=tr[class*='${admission}[card_no]']

    # Test the deletion of the admitted student.
    Click Element   css=button[type="submit"][title="Delete"]
    Wait Until Page Contains    Are you sure?
    Click Element   css=.swal2-confirm.swal2-styled
    Capture Page Screenshot
    Wait Until Page Does Not Contain    Are you sure?
    Wait Until Page Contains    Admission record deleted
    Sleep   30s
    Page Should Not Contain Element     css=tr[class*='${admission}[card_no]']
    
    # Check if the admission record was really deleted.
    Element Should Contain  css=button[name="show_admitted"]    Show Admitted Students
    Scroll Element Into View    css=.breadcrumb   
    Click Element   css=button[name="show_admitted"][value="1"]
    Wait Until Page Contains Element    css=button[name="show_admitted"][value="0"]
    Element Should Contain  css=button[name="show_admitted"]    Hide Admitted Students
    Log     ${admission}
    
    Capture Page Screenshot
    Page Should Not Contain     ${acode}
    Page Should Not Contain Element     css=tr[class*='${admission}[card_no]']

    
    
    