FROM ypasmk/robot-framework

MAINTAINER "Oshane Bailey" <b4.oshany@gmail.com>
LABEL name="Docker build for acceptance testing using the robot framework"

ENV	ROBOT_SITE_URL	http://localhost:8080

COPY output /output
COPY reports /reports
COPY scripts /scripts
COPY suites /suites

CMD ["/scripts/run_suite.sh"]

