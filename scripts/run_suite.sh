#!/usr/bin/env bash
set -e

if [ -z "$ROBOT_SITE_URL" ]; then
    ROBOT_VARIABLES=""
else
    ROBOT_VARIABLES=" --variable SITE_URL:$ROBOT_SITE_URL"
fi


if [ -z "$TEST_SUITE" ]; then
    TEST_SUITE=""
fi

pip install Faker==3.0.1

CMD="robot $ROBOT_VARIABLES --console verbose --outputdir /reports /suites/$TEST_SUITE"

echo ${CMD}

``${CMD}``
